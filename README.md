# VRChef

## General Information

VRChef is our senior project, developed at **Graphics Lab of TOBB University of Economics & Technology, Turkey**. We developed the project as a team consists of 4 people:

- Doğa Can YANIKOĞLU
- Muhammed Ertuğrul GÜNGÖR
- Seçil USTA
- Fatma ASAL

Project's aim is creating an unique experience based on VR where possible chef candidates can learn new recipes and improve their cooking skills by praticing on them in a realistic kitchen environment. 

Unity, Oculus Rift Headset, and Oculus Touch controllers are used in the development environment.

## Main Features

- Food Manipulation
    - Chopping
    - Cooking
    - Grilling
    - Boiling
    - Squeezing
    - Smashing
    - Mixing
    - Also a liquid module used along with features above to make them more realistic.
    
- VR Ready Kitchen Scene
    - A commercial typed kitchen scene is used to give it a serious and realistic look.
    - Placed many interactable kitchen equipments in the scene.
    - Drawers, cupboards, fridge and buttons in kitchen scene are fully interactable by Oculus Touch Controllers.
    - Player can teleport to different parts of the kitchen scene.
    - Some specific equipment in the scene can be broken by an impact.
    
- Recipe System
    - An advanced recipe data structure which can store unlimited combinations of foods and actions pre-defined in the project.
    - A VR user interface for player to easily create a recipe step-by-step, and apply them in kitchen scene on-the-go.
    - Created recipes can be saved to/loaded from hard disk.
    - A progress tracking module which shows real time directions to assist player along the cooking process.
    
## Trailer

[![VRChef | Trailer](https://img.youtube.com/vi/jr61hF-0TLo/0.jpg)](https://youtu.be/jr61hF-0TLo "VRChef | Trailer")

## Screenshots From Development

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot33.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot18.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot23.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot39.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot7.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot24.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot28.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot17.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot25.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot26.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot9.png "Screenshot")

![Screenshot](https://www.imageupload.co.uk/images/2018/06/13/Screenshot42.png "Screenshot")

## License

Copyright 2018 **UntitledGroup.txt (**

**- Doğa Can YANIKOĞLU**

**- Seçil USTA**

**- Muhammed Ertuğrul GÜNGÖR**

**- Fatma ASAL**

**)**

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

`http://www.apache.org/licenses/LICENSE-2.0`

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.